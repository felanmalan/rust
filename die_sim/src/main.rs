use std::io;
use rand::Rng;


const DIE_SIZE: u32 = 6;
const DIE_AMOUNT: usize = 5;
const THROW_SIZE: u32 = 100000;


fn main() -> io::Result<()> {


        /*
    [0] = total
    [1] = double
    [2] = triple
    [3] = quadrouple
    [4] = quintouple
        */
    let mut totals = [0.0; 5];

    println!("How many batches?");

    let mut batches = String::new();

    io::stdin()
        .read_line(&mut batches)
        .expect("Failed to read line");

    println!();

    let batches: u32 = batches
        .trim().parse()
        .expect("Use a integer!");

    while totals[0] < THROW_SIZE as f32 * batches as f32 {
        let progress = (totals[0] * 100.0) / (THROW_SIZE * batches) as f32;
        let progress: String = progress.to_string();

        println!("{}% Complete", progress);

        totals = throw_die(totals);
        
        // println!("{}%",(totals[0] * 100.0) / (THROW_SIZE * batches) as f32);

    }

    percentages(totals);
    
    Ok(())
}

fn percentages(totals: [f32; 5]) {
    let mut percentages = [0.0; 4];

    percentages[0] = totals[1] / totals[0] * 100.0;
    percentages[1] = totals[2] / totals[0] * 100.0;
    percentages[2] = totals[3] / totals[0] * 100.0;
    percentages[3] = totals[4] / totals[0] * 100.0;

    println!();
    println!("Total rolls = {} M", totals[0] / 1000000.0);
    println!("Doubles: {}%", percentages[0]);
    println!("Triples: {}%", percentages[1]);
    println!("Quadrouples: {}%", percentages[2]);
    println!("Quintouples: {}%", percentages[3]);
    
/*  let mut percantage_as_string = [""; 4];
    for i in 0..3 {
        percantage_as_string[i] = &percentages[i].to_string();
    } */
}

fn throw_die(mut totals: [f32; 5]) -> [f32; 5] {
    for _i in 0..THROW_SIZE {
        let mut die = [0; DIE_AMOUNT];
        for i in 0..(DIE_AMOUNT) {
            die[i] = rand::thread_rng().gen_range(1..=DIE_SIZE);
        }
        
        let mut add = [false; 4];
        for i in 0..DIE_AMOUNT {
            for n in 0..DIE_AMOUNT {
                if i == n { break; }
                else if die[i] == die[n] {
                    add[0] = true;
                } else {break;}

                for x in 0..DIE_AMOUNT {
                    if i == x || n == x {break;}
                    else if die[i] == die[x] {
                        add[1] = true;
                    } else {break;}

                    for y in 0..DIE_AMOUNT {
                        if i == y || x == y || n == y {break;}
                        else if die[i] == die[y] {
                            add[2] = true;
                        } else {break;}
                    }
                }
            }
        }

        if die[0] == die[1] && die[1] == die[2] && die[2] == die[3] && die[3] == die[4] {
            add[3] = true;
        }

        totals[0] += 1.0;
        if add[0] {totals[1] += 1.0}
        if add[1] {totals[2] += 1.0}
        if add [2] {totals[3] += 1.0}
        if add [3] {totals[4] += 1.0}
    }

    return totals
}