use std::io;

fn main() {
    println!("Welcome to My Text-Based RPG!");

    loop {
        // Display game options and prompt for input
        println!("1. Start a new game");
        println!("2. Load a saved game");
        println!("3. Exit");
        println!("Enter your choice: ");

        let mut choice = String::new();
        io::stdin()
            .read_line(&mut choice)
            .expect("Failed to read line");

        match choice.trim() {
            "1" => start_new_game(),
            "2" => load_saved_game(),
            "3" => {
                println!("Goodbye!");
                break;
            }
            _ => println!("Invalid choice. Please try again."),
        }
    }
}

struct Player {
    name: String,
    hp: i32,
    phys: i32,
    ment: i32,
}

fn start_new_game() {
    let mut player = Player {
        name: "temp".to_string(),
        hp: 20,
        phys: 5,
        ment: 5,
    };

    let mut choice = String::new();
    println!("What is your name?");
    io::stdin()
            .read_line(&mut choice)
            .expect("Failed to read line");
    
    player.name = choice.to_string();

    println!("You are:");
    println!("1) a physical fighter");
    println!("2) a mental fighter");
    println!("3) a balanced fighter");

    choice = String::new();
    io::stdin()
            .read_line(&mut choice)
            .expect("Failed to read line");
    
    match choice.trim() {
        "1" => {player.phys += 10; player.hp += 10},
        "2" => player.ment += 10,
        "3" => {player.phys += 5; player.ment += 5; player.hp += 5},
        
        _ => println!("Secret hard mode enabled.")
    }

    game_loop(player)
}

fn game_loop(player: Player) {
    loop {
        
    }
}

fn load_saved_game() {
    // Implement the logic to load a saved game here
}
