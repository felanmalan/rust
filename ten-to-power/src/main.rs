use std::io;

fn main() {

    println!("How old are you?");
    print!(">");

    let mut age_in_years = String::new();

    io::stdin().read_line(&mut age_in_years).expect("Failed to read line");

    println!();

    let age_in_years: f32 = age_in_years.trim().parse().expect("Enter a float (1.0, 3.7)");

    let mut age_in_second = 24.0 * 60.0 * 60.0; // Day

    age_in_second = age_in_second * 365.25; // Year

    age_in_second = age_in_second * age_in_years; // Aeg in years → seconds

    println!("Age in seconds is ~{age_in_second}");
}
